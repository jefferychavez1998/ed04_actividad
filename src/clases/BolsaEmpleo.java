package clases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * 
 * @author jeffe_ik8b4db
 *
 */
public class BolsaEmpleo {
	private List<Persona> listaVacantes;
	private String nombreBolsa;
	/**
	 * 
	 * @param listaVacantes
	 * @param nombreBolsa
	 */
	public BolsaEmpleo(List<Persona> listaVacantes, String nombreBolsa) {
		this.listaVacantes = new ArrayList<Persona>();
		this.nombreBolsa = nombreBolsa;
	}
	/**
	 * 
	 * @return
	 */
	public List<Persona> getListaVacantes() {
		return listaVacantes;
	}
	/**
	 * 
	 * @param listaVacantes
	 */
	public void setListaVacantes(List<Persona> listaVacantes) {
		this.listaVacantes = listaVacantes;
	}
	/**
	 * 
	 * @return
	 */
	public String getNombreBolsa() {
		return nombreBolsa;
	}
	/**
	 * 
	 * @param nombreBolsa
	 */
	public void setNombreBolsa(String nombreBolsa) {
		this.nombreBolsa = nombreBolsa;
	}

// Método que añade una persona a la bolsa de trabajo si es mayor de edad y no se encuentra en ninguna otra bolsa de trabajo
	/**
	 * 
	 * @param persona
	 * @param listaDeBolsas
	 */
	public void anyadirPersona(Persona persona, List<BolsaEmpleo> listaDeBolsas) {

		
		if (persona.obtenerEdad() >= 18) {
			//boolean estaEnBolsa = estaEnBolsa(persona, listaDeBolsas);
			if (!estaEnBolsa(persona, listaDeBolsas)) {
				this.listaVacantes.add(persona);
			}
		}
	}
	/**
	 * 
	 * @param persona
	 * @param listaDeBolsas
	 * @return
	 */
	public boolean estaEnBolsa(Persona persona, List<BolsaEmpleo> listaDeBolsas) {
		boolean estaEnBolsa = false;
		for (BolsaEmpleo bolsa : listaDeBolsas) {
			if (bolsa.listaVacantes.contains(persona)) {
				estaEnBolsa = true;
			}
		}
		return estaEnBolsa;
	}

	@Override
	/**
	 * 
	 */
	public String toString() {
		return "Bolsa de empleo: " + nombreBolsa + "\n" + Arrays.toString(this.listaVacantes.toArray());
	}

}
